import 'package:flutter/material.dart';
import 'package:pesantrenpondokquran/widgets/drawer_menu.dart';

class CustomDrawer extends StatelessWidget {
  // final loginStore;
  // final User user;

  CustomDrawer({
    Key? key,
    // this.loginStore,
    // this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final logo = Container(
      margin: EdgeInsets.only(bottom: 20.0),
      height: screenHeight * 0.2,
      alignment: Alignment.center,
      child: Icon(Icons.home),
      // child: Image.asset(
      //   kLogoImage,
      //   fit: BoxFit.cover,
      // ),
    );

    return Column(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(
              top: 50,
              left: 20.0,
              right: 20.0,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  logo,
                  DrawerMenu(
                    icon: Icon(Icons.flash_auto),
                    title: 'Menu 1',
                  ),
                  DrawerMenu(
                    icon: Icon(Icons.flash_auto),
                    title: 'Menu 1',
                  ),
                  DrawerMenu(
                    icon: Icon(Icons.flash_auto),
                    title: 'Menu 1',
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
