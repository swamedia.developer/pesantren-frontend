import 'package:flutter/material.dart';

class DrawerMenu extends StatelessWidget {
  final Icon? icon;
  final String? title;
  final VoidCallback? onTap;

  const DrawerMenu({
    Key? key,
    this.icon,
    this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        leading: icon,
        trailing: Icon(Icons.arrow_forward_ios),
        title: Text(title!),
        onTap: onTap,
      ),
    );
  }
}
